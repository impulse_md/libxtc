#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 10-01-2020
'''
import os
import os.path as op
import sys
import sysconfig

# Based on https://github.com/wichert/pybind11-example
try:
    from setuptools import setup, Extension
    from setuptools.command.build_ext import build_ext
    use_setuptools = True
except ImportError:
    from distutils.core import setup, Extension
    from distutils.command.build_ext import build_ext
    use_setuptools = False
from distutils.core import Command
from distutils.dep_util import newer_group
from distutils import errors

src_tmpl = '''
{0}
int main(){{
{1}
}}
'''
src_empty = src_tmpl.format("", "")


class BuildExt(build_ext):
    """A custom build extension for adding compiler-specific options."""
    c_opts = {
        'msvc': ['/EHsc'],
        'unix': [],
    }

    if sys.platform == 'darwin':
        c_opts['unix'] += ['-stdlib=libc++', '-mmacosx-version-min=10.6']

    def has_flag(self, flags, src):
        with open(self.tmpct, 'wt') as f:
            f.write(src)
            f.flush()
            extra_postargs = []
            if flags:
                extra_postargs = [flags]
            try:
                self.compiler.compile([f.name], output_dir="", extra_postargs=extra_postargs)
            except errors.CompileError:
                return False
        return True

    def has_int128(self):
        return self.has_flag("", src_tmpl.format("", "__int128 a;"))

    def omp_flag(self):
        opts = ["-fopenmp", " -openmp", "-qsmp=omp", "-Qopenmp", " -xopenmp", "-mp"]
        for flg in opts:
            if self.has_flag(flg, src_empty):
                return flg

    def cpp_flag(self):
        if self.has_flag('-std=c++11', src_empty):
            return '-std=c++11'
        else:
            raise RuntimeError('Unsupported compiler - at least C++11 support is required!')

    def build_extensions(self):
        if not self.force:
            skip_build = True
            for ext in self.extensions:
                sources = list(ext.sources)
                ext_path = self.get_ext_fullpath(ext.name)
                depends = sources + ext.depends
                if newer_group(depends, ext_path, 'newer'):
                    skip_build = False
                    break
            if skip_build:
                return
        extra_link_args = []
        ct = self.compiler.compiler_type
        opts = list(self.c_opts.get(ct, []))
        opts += ["-O3"]
        if not os.path.exists(self.build_temp):
            os.makedirs(self.build_temp)
        tmpct = os.path.join(self.build_temp, '_conftest')
        self.tmpct = tmpct + '.cpp'

        try:
            omp = self.omp_flag()
            if omp:
                opts += [omp]
                extra_link_args += [omp]

            if self.has_flag('', src_tmpl.format("", "__int128 a;")):
                opts += ["-DHAS_INT128"]

            for flag in ['-fno-exceptions', '-fno-rtti', '-ffast-math']:
                if self.has_flag(flag, src_empty):
                    opts.append(flag)

            if ct == 'unix':
                ml = os.getenv("MANYLINUX")
                opts.append(self.cpp_flag())
                if self.has_flag('-fvisibility=hidden', src_empty):
                    opts.extend(('-fvisibility=hidden', '-fvisibility-inlines-hidden'))
                # TODO: check link is possible! compile - not enougth
#                 if ml and self.has_flag('-static-libstdc++', src_empty):
#                     extra_link_args.append('-static-libstdc++')
#                     pass
                if not ml and self.has_flag('-march=native', src_empty):
                    opts.append('-march=native')
        finally:
            try:
                os.remove(self.tmpct)
                os.remove(tmpct + ".o")
            except:
                pass

        for ext in self.extensions:
            ext.extra_compile_args = opts
            ext.extra_link_args = extra_link_args
        build_ext.build_extensions(self)

    def get_libraries(self, ext):
        return []

    def get_ext_filename(self, fullname):
        so_ext = sysconfig.get_config_var('EXT_SUFFIX')
        if not so_ext:
            so_ext = sysconfig.get_config_var('SO')

        so_ext = so_ext[so_ext.rfind("."):]
        return fullname + so_ext


try:
    # A hack to change output tag name
    from wheel.bdist_wheel import bdist_wheel as _bdist_wheel

    class bdist_wheel(_bdist_wheel):

        def get_tag(self):
            tag = _bdist_wheel.get_tag(self)
            return ('py2.py3', 'none', tag[2])

except ImportError:
    bdist_wheel = None


class test(Command):
    user_options = [
        ('build-base=', 'b',
         "base directory for build library"),
    ]

    def initialize_options(self):
        self.build_base = 'build'
        pass

    def finalize_options(self):
        pass

    def run(self):
        import subprocess
        from pkg_resources import normalize_path

        self.run_command('egg_info')

        # Build extensions
        self.reinitialize_command('build', build_base=self.build_base)  # , inplace=1
        self.reinitialize_command('build_py')
        self.reinitialize_command('build_ext')
        self.run_command('build')
        bpy_cmd = self.get_finalized_command("build_py")
        build_path = normalize_path(bpy_cmd.build_lib)

        ei_cmd = self.get_finalized_command("egg_info")
        project_path = normalize_path(ei_cmd.egg_base)
        subprocess.call([sys.executable, "-m", "runner"], cwd=op.join(project_path, "tests"), env={"PYTHONPATH":build_path})


VERSION = "1.0.0"
CLASSIFIERS = [
    "Development Status :: 4 - Beta",
    "Intended Audience :: Science/Research",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
    "Programming Language :: C++",
    "Programming Language :: Python",
    "Programming Language :: Python :: 2",
    "Programming Language :: Python :: 3",
    "Topic :: Scientific/Engineering :: Bio-Informatics",
    "Operating System :: Microsoft :: Windows",
    "Operating System :: POSIX",
    "Operating System :: Unix",
    "Operating System :: MacOS",
]

directory = op.abspath(op.dirname(__file__))
with open(op.join(directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


def main():

    kw = {}
    if use_setuptools:
        kw["install_requires"] = ["numpy"]
    else:
        kw["requires"] = "numpy"

    description = "Lightweight xtc-trajectory reader."
    setup(name="libxtc",
               author="Nikolay A. Krylov",
               author_email="krylovna@gmail.com",
               url="https://gitlab.com/impulse_md/libxtc",
               description=description,
               long_description=long_description,
               version=VERSION,
               classifiers=CLASSIFIERS,
               license="GPL3",

               packages=['libxtc'],
               cmdclass={
                    'build_ext': BuildExt,
                    'bdist_wheel': bdist_wheel,
                    'test': test,
               },
               ext_modules=[
                   Extension("libxtc._xtc",
                            sources=["libxtc/xtc_unpack.cpp"],
                            export_symbols=["unpack_frame"],
                            language='c++',
                   )
               ],
               **kw
               )


if __name__ == "__main__":
    main()
