libxtc is a lightweight trajectory reader based on code from [xdrfile library (GROMACS)](http://www.gromacs.org/Downloads_of_outdated_releases/) with improved performance.


Installing libxtc
-----------------
The libxtc library supports Python 2 and 3 under Linux, Windows and macOS.
Pre-build binary packages are available for x64_86 architecture.
Minimal supported OS versions are: Windows 7+, macOS 10.6+, Linux with glibc 2.5+

Binary installation
-------------------
Check `pip` package manager version. Versions above 8.1 are supported.

Make sure `wheel` package is installed, using your package manager or following command:
`python -m pip install wheel [--user]`

Then download `*.whl` file for your platform from [**Releases**](https://gitlab.com/impulse_md/libxtc/releases) page and install with pip:
`python -m pip install <downloaded libxtc package>.whl [--user]`

Source installation
-----------------
Download sources with git or as zip-archive (and unzip if necessary).
Make sure c++ compiler is installed. gcc or clang is prefered. Other compilers were not tested, but should also work. Next, run a command in source directory:
`python -m pip install . [--user]`.


Using libxtc
------------
The libxtc module contains a single class 'XTCReader' that mediates access to all MD trajectory data.
Its constructor takes optional parameters that specify the required time range, timestep, and number of threads (beg, end, dt, and nt, respectively).
If beg, end or dt parameter is not set, default values from input MD trajectory file will be used.

Since the XTCReader class supports context management and generator protocols, the frame reading loop can be implemented as follows:

```python
import libxtc

rd=libxtc.XTCReader(fname, beg=1000, dt=100, nt=4)

for x,b,t in rd:
 print(b,t)
```

Similar results can be obtained using `next_frame` and `eot` methods of the XTCReader class:

```python
while 1:
 rd.next_frame()
 print(rd.b,rd.t)
 if rd.eot():
  break
```

Examples
--------
The examples directory contains executables implementing simple trajectory data processing and a trajectory file for them.


Please note
-----------
- No units conversion of input parameters or xtc frame data is performed.
- Frame skip/search implementation assumes that trajectory data contains frames with the increasing time. Otherwise, beg,end and dt parameters can't be used. However, full trajectory processing is still possible.


Citing information
------------------
If your find this code usefull, please cite: "libxtc: an efficient library for reading XTC‑compressed MD trajectory data",
https://doi.org/10.1186/s13104-021-05536-5.


License
-------

Copyright &copy; 2016-2020 Nikolay A. Krylov
All rights reserved.

The libxtc is a free software package, distributed under GPLv3 license. See the file LICENSE for more details.
