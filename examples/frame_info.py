#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2020, Nikolay A. Krylov
# Created on 27.10.20

# This example program shows time, cell vectors and atom coordinate
# ranges for every frame in given xtc file.

from __future__ import print_function, unicode_literals
import sys
import os.path as op
import libxtc


def main():
    if len(sys.argv) < 2:
        print("Usage: {} <xtc file name>".format(op.basename(__file__)))
        return
    xtcr = libxtc. XTCReader(sys.argv[1])
    for i, (X, b, t) in enumerate(xtcr):
        print('i',i,'time',t, "a",b[0],"b",b[1],"c",b[2],"cmin", X.min(axis=0),"cmax", X.max(axis=0) )
        i += 1

if __name__ == "__main__":
    main()
