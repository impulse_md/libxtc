#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2020, Nikolay A. Krylov
# Created on 7.04.20

#This example program estimates xtc file reading rate.
#Begin and/or end time, time step and cpu count can be specified.
#Input file is the first argument and begin,end,dt,ncpu - second one (if present).
#All 4 elements in the second argument must be listed and they are comma-delimited.

import sys
import os
import timeit
import libxtc

def main():

        fnm = sys.argv[1]
        #TODO: , dt=dt,nt=nt
        b=e=dt=-1
        nt=4
        if len(sys.argv)>2:
            parts=sys.argv[2].split(",")
            b,e,dt,nt=[float(v) for v in parts[:3]]+[int(parts[3])]
        rd =libxtc. XTCReader(fnm,beg=b,end=e,dt=dt,nt=nt)
        start = timeit.default_timer()
        i=0
        for _ in rd:
            i += 1

        end = timeit.default_timer()
        dt = end - start

        print("Perf. stat. for  {}.  nt: {} na: {} nframes: {} read rate: {:g} frame/s".format(fnm, nt, rd.nacur, i,  i / dt))

if __name__ == "__main__":
    main()
