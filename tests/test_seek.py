# -*- coding: utf-8 -*-

import os.path as op
import unittest
import libxtc
import numpy as np

data_dir = op.join(op.dirname(__file__), "data")


class SeekTest(unittest.TestCase):

    def _dotest(self, beg, e, dt, times):
        tlist = []
        with libxtc.XTCReader(op.join(data_dir, self.filename), beg=beg, end=e, dt=dt) as f:
            for x, b, t in f:
                tlist.append(f.t)
        nptimes = np.array(times, dtype=np.float32)
        nptlist = np.array(tlist, dtype=np.float32)
        self.assertEqual(len(nptlist), len(nptimes), "Frame time list size mismatch for {} {} {}".format(beg, e, dt))
        self.assertTrue(np.all(np.abs(nptimes - nptlist) < 1e-7) , "Frame time list mismatch for {} {} {}".format(beg, e, dt))

    def test_seek_one_frame(self):
        self.filename = "f1.xtc"
        self._dotest(-1, -1, -1, [2])  # single frame
        self._dotest(3, -1, -1, [])  # outside range
        self._dotest(-1, 1, -1, [])  # -//-

    def test_seek(self):
        self.filename = "pep.xtc"

        tl = [0.1 * i for i in range(21)]  # add time

        self._dotest(-1, -1, -1, tl)  # all frames
        self._dotest(-1, -1, 1, tl[::10])  # dt=1
        self._dotest(-1, -1, 0.2, tl[::2])  # dt=0.2

        idx1 = tl.index(1)
        self._dotest(1, -1, -1, tl[idx1:])  # b
        self._dotest(1.05, -1, -1, tl[tl.index(1.1):])  # b!=tframe
        idx18 = tl.index(1.8) + 1

        self._dotest(-1, 0.4, -1, tl[: 5])  # end on frame
        self._dotest(-1, 1.85, -1, tl[:idx18])  # e!=tframe

        self._dotest(-1, 0.25, -1, tl[:3])  # e!=tframe
        self._dotest(-1, 1.8, 0.5, tl[:idx18:5])  # e+dt
        self._dotest(-1, 1, 0.25, [0., 0.3 , 0.5 , 0.8 , 1.  ])  # e+dt!=tframe

        self._dotest(1, -1, 1, tl[idx1::10])  # b+dt
        self._dotest(1.5, -1, 0.3, [1.5, 1.8])  # b+dt!= trj dt
        self._dotest(1, 3, 0.5, tl[idx1::5])  # b+e+dt
        self._dotest(1, 1.85, 0.3, tl[idx1::3][:3])  # b+e+dt!=trj dt
