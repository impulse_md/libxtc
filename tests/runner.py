# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals, absolute_import, division
import unittest

import sys
import os.path as op

testdir = op.dirname(op.abspath(__file__))
sys.path.append(testdir)

if __name__ == "__main__":
    testsuite = unittest.TestLoader().discover(testdir)
    unittest.TextTestRunner(verbosity=2).run(testsuite)
