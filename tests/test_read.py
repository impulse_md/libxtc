# -*- coding: utf-8 -*-

import os.path as op
import unittest
import  libxtc
import  numpy as np

data_dir = op.join(op.dirname(__file__), "data")

# NOTE: bad_frame.xtc was build from 3064-5-95-58.xtc, reported here
# https://github.com/mdtraj/mdtraj/pull/607/commits/f52fb980a41de17021c5b34969450ba91cf6b396


class ReadTest(unittest.TestCase):

    def test_nonxtc(self):
        with self.assertRaises(IOError):
            libxtc.XTCReader(__file__)

    def test_read(self):
        f1crd = np.load(op.join(data_dir, "pep.npy"))
        with libxtc.XTCReader(op.join(data_dir, "pep.xtc"), nt=1) as rd:
            i = 0
            for x, b, t in rd:
                self.assertTrue(np.all(rd.box.diagonal() > 0), "Bad box element at time {}".format(rd.t))
                self.assertEqual((rd.box > 0).sum(), 3, "Bad box vectors at time {}".format(rd.t))
                d = np.linalg.norm(rd.X - f1crd[i], axis=1)
                self.assertTrue(np.all(d < 1e-3), "Bad frame at time {}".format(rd.t))
                i += 1

    def test_bad_frame(self):
        # https://github.com/mdtraj/mdtraj/issues/606
        with self.assertRaises(IOError):
            with libxtc.XTCReader(op.join(data_dir, "bad_frame.xtc")) as rd:
                for _ in rd:
                    pass
